import { Component, OnInit, EventEmitter,Output  } from '@angular/core';
import { ImageSrcService } from 'src/app/image-src.service';
import { InpaintingService } from 'src/app/inpainting.service';
import { Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';
@Component({
  selector: 'app-paint',
  templateUrl: './paint.component.html',
  styleUrls: ['./paint.component.css']
})
export class PaintComponent implements OnInit {
  data:any = {src: ""};
  imageSrc:any;
  state: boolean = false;
  constructor(private router:Router,
    private service:ImageSrcService,private servicePaint:InpaintingService) { 
  }

  ngOnInit(): void {
    
  }
  changeComponent(){
    this.data.src= this.imageSrc;
    if (this.data.src!=""){
      this.service.setImageOriginal(this.data);
      this.servicePaint.sendUrl(this.imageSrc)//.subscribe((res: any) => {});
      this.router.navigate(["draw"]);//redirects url to new component
    }
    
}
  chooseImage(){
    document.getElementById('input')?.click()
  }
  getUrl(event:any){
    this.state= true;
    console.log(event.target.files[0])
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]); 
    reader.onload = (_event) => { 
      this.imageSrc = reader.result; 
      console.log(this.imageSrc)
    }
    
  }
  changeState(){
    this.state = false;
  }

}
