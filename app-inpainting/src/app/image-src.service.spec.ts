import { TestBed } from '@angular/core/testing';

import { ImageSrcService } from './image-src.service';

describe('ImageSrcService', () => {
  let service: ImageSrcService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImageSrcService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
