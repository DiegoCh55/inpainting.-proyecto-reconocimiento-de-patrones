import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-show-model',
  templateUrl: './show-model.component.html',
  styleUrls: ['./show-model.component.css']
})
export class ShowModelComponent implements OnInit {
  color:string = "teal"
  resultLabel:string = "Esperando resultados"
  display:string = "none"
  points : string = ""
  interval ! : any
  resultImg : string = ""

  constructor(private router:Router, private change:ChangeDetectorRef) { }

  ngOnInit(): void {
    
    this.waiting()
   
  }
  
  async waiting(){
    this.interval = await setInterval(() => {
      if(this.points != "...") 
        this.points += "."
      else{
        this.resultImg = "assets/result.jpg"
        this.change.detectChanges()
        this.display = "flex"
        this.color = "black"
        this.resultLabel = "Resultado"
        this.points = ""
        
        clearInterval(this.interval)
      }

    },700)
  }


  tryAgain(){
    this.router.navigateByUrl('paint')
  }

}
