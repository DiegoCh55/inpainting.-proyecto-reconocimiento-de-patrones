import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PaintComponent } from './paint/paint.component';
import {MatIconModule} from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import { ImageDrawingModule } from 'ngx-image-drawing';
import { DrawImageComponent } from './draw-image/draw-image.component';
import { ShowModelComponent } from './show-model/show-model.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { SignaturePadModule } from 'angular2-signaturepad';
import {HttpClientModule} from "@angular/common/http"
@NgModule({
  declarations: [
    AppComponent,
    PaintComponent,
    DrawImageComponent,
    ShowModelComponent
  ],
  imports: [
    ImageDrawingModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule, 
    MatIconModule,MatButtonModule,SignaturePadModule,MatSnackBarModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
