import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { PaintComponent } from './paint/paint.component';
import { DrawImageComponent } from './draw-image/draw-image.component';
import { ShowModelComponent } from './show-model/show-model.component';
const routes: Routes = [
  {path: "", redirectTo: "paint", pathMatch: "full"},
  {path: "Main", component: AppComponent, 
  children: []},
    
    {path: "draw", component: DrawImageComponent},
    {path: "paint", component: PaintComponent},
    {path: "model", component: ShowModelComponent},
    {path: '**', pathMatch: 'full', redirectTo: 'paint'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
