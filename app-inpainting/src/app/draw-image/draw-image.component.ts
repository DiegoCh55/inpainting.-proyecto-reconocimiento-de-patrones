import { Component, OnInit, ViewChild , ElementRef, AfterViewInit} from '@angular/core';
import { Router }         from '@angular/router';
import { InpaintingService } from 'src/app/inpainting.service'
import SignaturePad from 'signature_pad';

@Component({
  selector: 'app-draw-image',
  templateUrl: './draw-image.component.html',
  styleUrls: ['./draw-image.component.css'],
})
export class DrawImageComponent implements OnInit, AfterViewInit {
  imageSrc!: string;
  signaturePadOptions: Object = { 
    'minWidth': 5,
    'canvasWidth': 550,
    'canvasHeight': 550
  };
  
  @ViewChild('sPad', {static: true}) signaturePadElement: any;
  signaturePad: any
  constructor(private router:Router,
    private servicePaint: InpaintingService
    ) { }

  ngOnInit(): void {
    
    //this.data = this.service.getImageOriginal();
    //this.imageSrc = this.data.src;
    //console.log(this.data)
    //console.log(this.imageSrc)
  }
  ngAfterViewInit(): void {
    this.signaturePad = new SignaturePad(this.signaturePadElement.nativeElement);
    this.signaturePad.penColor = "rgb(255,255,255)";
  }

  saveJPG() {
    if (this.signaturePad.isEmpty()) {
      alert('Dibuje sobre la imagen para poder continuar');
    } else {
      //this.servicePaint.sendUrl(this.imageSrc).subscribe((res: any) => {});
      const dataURL = this.signaturePad.toDataURL('image/jpeg');
      this.servicePaint.sendMask(dataURL)//.subscribe((res: any) => {});;
      this.servicePaint.startInpainting()//.subscribe((res: any) => {});;
      this.changeComponent('model')
    }
  }

  
  changeComponent(page: string){
  
    this.router.navigate([page]);//redirects url to new component
    
  }


}
