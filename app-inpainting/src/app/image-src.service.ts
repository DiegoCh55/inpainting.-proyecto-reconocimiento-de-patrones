import { Injectable, Component } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageSrcService {
  private imageSrc:any = undefined;

    setImageOriginal(src:any){
        this.imageSrc = src;
    }

    getImageOriginal():any{
        return this.imageSrc;
    }
}
