import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class InpaintingService {

  dir = 'http://127.0.0.1:5000';
  constructor(private http: HttpClient) { 

  }
  
  async sendUrl(url:string){
   
    await fetch(`${this.dir}/resize?img=${encodeURIComponent(url)}`)//.then(function(response){return response.json})
    
  }

  async startInpainting(){
    
    await fetch(`${this.dir}/start-inpainting`)
  
  }

  async sendMask(url:string){
    
    await fetch(`${this.dir}/getMask?img=${encodeURIComponent(url)}`)
  
  }
 
}
