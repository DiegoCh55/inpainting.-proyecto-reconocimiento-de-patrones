import tensorflow as tf
import numpy as np
import cv2
import os

from matplotlib import pyplot as plt
from deepimageinpainting.utils.pconv_layer import PConv2D
from tensorflow import keras
from tensorflow.keras.models import load_model


def dice_coef(y_true, y_pred):
    y_true_f = keras.backend.flatten(y_true)
    y_pred_f = keras.backend.flatten(y_pred)
    intersection = keras.backend.sum(y_true_f * y_pred_f)
    return (2. * intersection) / (keras.backend.sum(y_true_f + y_pred_f))

def overlay_transparent(background, overlay, x, y):

    background_width = background.shape[1]
    background_height = background.shape[0]

    if x >= background_width or y >= background_height:
        return background

    h, w = overlay.shape[0], overlay.shape[1]

    if x + w > background_width:
        w = background_width - x
        overlay = overlay[:, :w]

    if y + h > background_height:
        h = background_height - y
        overlay = overlay[:h]

    if overlay.shape[2] < 4:
        overlay = np.concatenate(
            [
                overlay,
                np.ones((overlay.shape[0], overlay.shape[1], 1), dtype = overlay.dtype) * 255
            ],
            axis = 2,
        )

    overlay_image = overlay[..., :3]
    mask = overlay[..., 3:] / 255.0

    background[y:y+h, x:x+w] = (1.0 - mask) * background[y:y+h, x:x+w] + mask * overlay_image

    return background


def adaptInputs():
    img = cv2.imread('../app-inpainting/src/assets/src.jpg', cv2.IMREAD_UNCHANGED)
    mask = cv2.imread('../app-inpainting/src/assets/mask.jpg', cv2.COLOR_BGR2RGB)
    
    #img = cv2.resize(img, (256, 256), interpolation=cv2.INTER_AREA)
    mask = cv2.resize(mask, (256, 256), interpolation=cv2.INTER_AREA)
    mask[mask<128] = 0 
    mask[mask>128] = 255

    img[mask==0] = 255
    #tempMask = cv2.cvtColor(mask, cv2.COLOR_BGR2RGBA)
    #tempMask = cv2.bitwise_not(tempMask)
    #mask = cv2.bitwise_not(mask)

    #white_mask = tempMask[:, :, 0] <= 30
    #tempMask[white_mask] = [255, 255, 255, 0]

    #img = overlay_transparent(img,tempMask,0,0)

    cv2.imwrite('../app-inpainting/src/assets/maskedImg.jpg',img)

    return img/255,mask/255

def main():
    maskedImg, mask = adaptInputs()
    modelInputs = [maskedImg.reshape((1,256,256,3)), mask.reshape((1,256,256,3))]

    trainedModel = load_model('inpainting.h5', custom_objects={'PConv2D': PConv2D,'dice_coef':dice_coef})
    
    impaintedImage = trainedModel.predict(modelInputs)
    impaintedImage = impaintedImage.reshape(impaintedImage.shape[1:])
    impaintedImage = cv2.cvtColor(np.float32(impaintedImage), cv2.COLOR_BGR2RGB)
    impaintedImage = cv2.resize(impaintedImage, (512, 512), interpolation=cv2.INTER_AREA)

    plt.imsave('../app-inpainting/src/assets/result.jpg',impaintedImage)
    
    return True
