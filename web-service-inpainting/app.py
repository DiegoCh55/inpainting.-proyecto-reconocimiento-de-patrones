import urllib
from urllib.request import urlopen
from urllib.parse import unquote

from flask import Flask, request, jsonify ,make_response
from flask_cors import CORS, cross_origin
from main import main
from main import cv2
from main import np

app = Flask(__name__)
#app.config['SECRET_KEY'] = 'mecagoenCors'
#app.config['CORS_HEADERS'] = 'Content-Type'
CORS(app)

@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials")
    response.headers.add('Access-Control-Allow-Methods', "GET, POST, PUT, DELETE")
    response.headers.add("Access-Control-Allow-Credentials", "true")

    return response

'''
def start_inpainting():
    img = request.args.get('img')
    if(img == None):
        return jsonify({'Status': "Error"})
    else:
        return jsonify({'image': "Aqui deberia ir la imagen"})
'''

@app.route('/getMask', methods = ['GET', 'POST'])
def getMask():  
    
    imgPath = unquote(request.args['img'])
    
    req = urlopen(imgPath)
    arr = np.asarray(bytearray(req.read()), dtype=np.uint8)
    img = cv2.imdecode(arr, -1) # 'Load it as it is'
    img = cv2.bitwise_not(img)
    cv2.imwrite('../app-inpainting/src/assets/mask.jpg',img)
     
    response = jsonify({'Status':True})
    
    return  response

@app.route('/start-inpainting', methods = ['GET'])
def start_inpainting():  
    flag = main()
    
    if(flag == True):

        response = jsonify({'Status':True})
        return  response
    else:

        response = jsonify({'Status':False})
        return  response

@app.route('/resize', methods = ['GET','POST','OPTIONS'])
#@cross_origin( allow_headers = ['Content-Type'])
#@supports_credentials(True)
def resize():
    imgPath = unquote(request.args['img'])
   
    req = urlopen(imgPath)
    arr = np.asarray(bytearray(req.read()), dtype=np.uint8)
    img = cv2.imdecode(arr, -1) # 'Load it as it is'
    cv2.imwrite('../app-inpainting/src/assets/src.jpg',img)

    h, w, c = img.shape

    imgShow = None
    imgSrc = None

    if (h < 256 | w < 256):
        imgShow = cv2.resize(img, (512, 512), interpolation=cv2.INTER_LINEAR)
        imgSrc = cv2.resize(img, (256, 256), interpolation=cv2.INTER_LINEAR)
        
    else:
        imgShow = cv2.resize(img, (512, 512), interpolation=cv2.INTER_AREA)
        imgSrc = cv2.resize(img, (256, 256), interpolation=cv2.INTER_AREA)

    cv2.imwrite('../app-inpainting/src/assets/show.jpg', imgShow)
    cv2.imwrite('../app-inpainting/src/assets/src.jpg', imgSrc)
    
    response = jsonify({'Status':True})
    
    return  response
   
if __name__ == '__main__':
    app.run(debug=True, port=5000) #run app in debug mode on port 5000s


